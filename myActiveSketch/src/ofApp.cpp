#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofEnableDepthTest();
	ofBackground(0,100,100);
	ofSetFrameRate(24);
	//cam.setNearClip(0.1);
	//cam.setFarClip(100);
	cam.setPosition(
		0, -30,5
	);
	box.set(
		10, 10, 10
	);
	cam.tiltDeg(90);

	int numCols = 50;
	int numRows = 50;
	int spacing = 2;

	for (int i = -numCols / 2; i < numCols / 2; i++) {
		for (int j = -numRows / 2; j < numRows / 2; j++) {
			mesh.addVertex(ofPoint(i*spacing, j*spacing, 0
			));
			mesh.addColor(ofFloatColor(255
			));
		}
	}

	// Set up triangles' indices
// only loop to -1 so they don't connect back to the beginning of the row
	for (int x = 0; x < numCols - 1; x++) {
		for (int y = 0; y < numRows - 1; y++) {
			int topLeft = x + numCols * y;
			int bottomLeft = x + 1 + numCols * y;
			int topRight = x + numCols * (y + 1);
			int bottomRight = x + 1 + numCols * (y + 1);

			mesh.addTriangle(topLeft, bottomLeft, topRight);
			mesh.addTriangle(bottomLeft, topRight, bottomRight);

		}
	}
}

//--------------------------------------------------------------
void ofApp::update(){
	for (int i = 0; i < mesh.getVertices().size(); i++) {
		float x = mesh.getVertex(i).x ;
		float y = mesh.getVertex(i).y;

		mesh.setVertex(i, ofVec3f(x, y, getZnoisevalue(x, y)));

	}
	
}

//--------------------------------------------------------------
void ofApp::draw(){
	cam.begin();
	ofSetColor(0,0,200);
	box.setPosition(
		0, 0, getZnoisevalue(0,0)
	);
	box.draw();
	//mesh.drawVertices();
	mesh.drawWireframe();
	cam.end();


}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'w') {
		cam.dolly(10);
	}
	if (key == 's') {
		cam.dolly(-10);
	}
	if (key == 'd') {
		cam.pan(10);
	}
	if (key == 'a') {
		cam.pan(-10);
	}


}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
//---------------------------------------------------------------
float ofApp::getZnoisevalue(float x, float y) {
	float n1 = 0;
	float n2 = 0;

	float n1_freq = ofRandom(
		100
	);
	float n2_freq = ofRandom(
		-100
	);
	float n1_amp = ofNoise(10)* 10;
	float n2_amp = ofNoise(5) *5;
	n1 = ofSignedNoise(x * n1_freq, y * n1_freq, ofGetElapsedTimef()) * n1_amp;
		n2 = ofSignedNoise(x * n2_freq, y * n2_freq, ofGetElapsedTimef()) * n2_amp;
		return n1 + n2;
}
